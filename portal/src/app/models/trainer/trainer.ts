export interface Trainer {
    nickname: string;
    fullName: string;
    cpf: number;
    cnpj: number;
    birthdate: string;
    email: string;
    cellphone: number;
    zoomAccount: string;
    active: boolean;
}
